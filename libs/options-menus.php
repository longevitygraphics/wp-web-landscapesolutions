<?php
/**
 * Functions file that defines options and settings and menus for the theme.
 *
 * @package lg_template_a
 */

?>

<?php
/**
 * Global Variables
 */
$options = get_option( 'lg_template_service_archive_settings' );

$defaults = array(
	'lg-service-archive-banner'      => '',
	'lg-service-archive-banner-text' => '',
	'lg-service-archive-blurb'       => '',
);

$options = array_merge( $defaults, $options );


?>

<?php

/**
 * Adds Menu Items to Posts Menu
 */
function lg_template_init_menus() {
	add_submenu_page( 'edit.php', 'Service Categories', 'Service Categories', 'manage_options', 'edit-tags.php?taxonomy=lg-service-category', null, 80 );
	add_submenu_page( 'edit.php', 'Service Archive Page', 'Service Archive Page', 'manage_options', 'lg-service-archives', 'lg_render_service_archive_backend', 90 );
}



/**
 * Action that adds menus to WordPress Admin menu during admin_menu hook
 */
add_action( 'admin_menu', 'lg_template_init_menus' );


/**
 * Custom Options and Settings for Archive Service Page
 */
function lg_template_init_settings() {

	register_setting(
		'lg-service-archive',
		'lg_template_service_archive_settings',
		array(
			'type'              => 'array',
			'sanitize_callback' => null,
			'show_in_rest'      => false,
		)
	);

	add_settings_section(
		'lg_template_service_archive_options',
		__( 'Service Archive Options', 'lg-template' ),
		'lg_template_service_archive_options_cb',
		'lg-service-archive'
	);

	add_settings_field(
		'lg-service-archive-banner',
		__( 'Banner Image', 'lg-template' ),
		'lg_service_archive_banner_cb',
		'lg-service-archive',
		'lg_template_service_archive_options',
		array(
			'label_for' => 'lg-service-archive-banner',
		)
	);

	add_settings_field(
		'lg-service-archive-banner-text',
		__( 'Banner Text', 'lg-template' ),
		'lg_service_archive_banner_text_cb',
		'lg-service-archive',
		'lg_template_service_archive_options',
		array(
			'label_for' => 'lg-service-archive-banner-text',
		)
	);

	add_settings_field(
		'lg-service-archive-blurb',
		__( 'Archive Blurb', 'lg-template' ),
		'lg_service_archive_blurb_cb',
		'lg-service-archive',
		'lg_template_service_archive_options',
		array(
			'label_for' => 'lg-service-archive-blurb',
		)
	);
}

add_action( 'admin_init', 'lg_template_init_settings' );

/**
 * Callback to render Service Archive Options
 *
 * @param array $args Provides options for Settings Group.
 * // section callbacks can accept an $args parameter, which is an array.
 * // $args have the following keys defined: title, id, callback.
 * // the values are defined at the add_settings_section() function.
 */
function lg_template_service_archive_options_cb( $args ) {
	?>
	<!-- This is where you can place content above the group of settings-->
	<?php
}


/**
 * Callback to render Service Archive Banner settings field.
 *
 * @param array $args Provides options for Settings Field.
 * // section callbacks can accept an $args parameter, which is an array.
 * // $args is an array of values defined at add_settings_field, typically contains label_for.
 */
function lg_service_archive_banner_cb( $args ) {
	wp_enqueue_media();
	global $options;
	$lg_service_archive_banner     = $options['lg-service-archive-banner'];
	$lg_service_archive_banner_url = wp_get_attachment_image_src( $lg_service_archive_banner, 'full' );
	?>
	<div class="imgage-preview-wrapper">
		<img src="<?php echo esc_attr( $lg_service_archive_banner_url[0] ); ?>" alt="" id="image-preview" width='633' height:='100' style="max-height:100px; height: auto; max-width:100%; width: 100%">
	</div>
	<input id="upload_image_button" type="button" class="button" value="<?php echo __( 'Upload Image', 'lg-template' ); //phpcs:ignore  ?>" />
	<input 
		type="hidden" 
		name="lg_template_service_archive_settings[<?php echo esc_attr( $args['label_for'] ); ?>]"  
		id="<?php echo esc_attr( $args['label_for'] ); ?>" 
		value="<?php echo esc_attr( $lg_service_archive_banner ); ?>"
	>
	<?php
}




/**
 * Callback to render Service Archive Blurb settings field
 *
 * @param array $args Provides options for Settings Field.
 */
function lg_service_archive_banner_text_cb( $args ) {
	global $options;
	$lg_service_archive_banner_text = $options['lg-service-archive-banner-text'];
	?>
	<input 
		type="text"
		name="lg_template_service_archive_settings[<?php echo esc_attr( $args['label_for'] ); ?>]"  
		id="<?php echo esc_attr( $args['label_for'] ); ?>" 
		value="<?php echo esc_attr( $lg_service_archive_banner_text ); ?>"
	/>
	<?php
}

/**
 * Callback to render Service Archive Banner Text settings field
 *
 * @param array $args Provides options for Settings Field.
 */
function lg_service_archive_blurb_cb( $args ) {
	global $options;
	$lg_service_archive_blurb = $options['lg-service-archive-blurb'];
	?>
	<textarea
		name="lg_template_service_archive_settings[<?php echo esc_attr( $args['label_for'] ); ?>]"  
		id="<?php echo esc_attr( $args['label_for'] ); ?>" 
		style="width:100%; height: auto; min-height:125px"
	><?php echo esc_textarea( $lg_service_archive_blurb ); ?></textarea>
	<?php
}


/**
 * Renders Service Archive Menu Page in WP Backend
 * // Referenced by add_submenu_page @line 17
 */
function lg_render_service_archive_backend() {
	?>
	<?php
	// check user capacities.
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}

	if ( isset( $_GET['settings-updated'] ) ) { // phpcs:ignore
		// add settings saved message with the class of "updated".
		add_settings_error( 'lg_template_messages', 'lg_template_message', __( 'Settings Saved', 'lg-template' ), 'updated' );
	}

	settings_errors( 'lg_template_messages' );
	?>
	<div class="wrap">
	<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
	<form action="options.php" method="post">
	<?php
	// output security fields for the registered setting "lg-template".
	settings_fields( 'lg-service-archive' );
	// output setting sections and their fields
	// (sections are registered for "lg_tempalte", each field is registered to a specific section).
	do_settings_sections( 'lg-service-archive' );
	// output save settings button.
	submit_button( 'Save Settings' );
	?>
	</form>
	</div>
	<?php
}
