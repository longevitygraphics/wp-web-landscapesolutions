<?php
/**
 * Theme Support Declaration File
 *
 * @package lg_template
 */

?>
<?php

/**
 * Callback function that adds theme supports.
 */
function add_child_template_support() {
	/*add theme colors*/
	add_theme_support(
		'editor-color-palette',
		array(
			array(
				'name'  => __( 'Primary', 'wp-theme-parent' ),
				'slug'  => 'primary',
				'color' => '#58c5c9',
			),
			array(
				'name'  => __( 'Secondary', 'wp-theme-parent' ),
				'slug'  => 'secondary',
				'color' => '#544a3f',
			),
			array(
				'name'  => __( 'Accent', 'wp-theme-parent' ),
				'slug'  => 'accent',
				'color' => '#ec2027',
			),
			array(
				'name'  => __( 'White', 'wp-theme-parent' ),
				'slug'  => 'white',
				'color' => '#ffffff',
			),
			array(
				'name'  => __( 'Dark', 'wp-theme-parent' ),
				'slug'  => 'dark',
				'color' => '#333333',
			),
			array(
				'name'  => __( 'Light', 'wp-theme-parent' ),
				'slug'  => 'light',
				'color' => '#F8F8F8',
			),

		)
	);

	// add support for editor styles.
	add_theme_support( 'editor-styles' );

}

add_action( 'after_setup_theme', 'add_child_template_support', 20 );
