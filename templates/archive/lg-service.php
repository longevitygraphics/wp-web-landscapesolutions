<?php
/**
 * Service Archive Page Template file
 * @phpcs:disable Squiz.ControlStructures.ControlSignature.NewlineAfterOpenBrace
 *
 * @package lg_template_a
 */

?>

<?php get_header(); ?>
<?php

$archive_options = get_option( 'lg_template_service_archive_settings' );
$banner          = isset( $archive_options['lg-service-archive-banner'] ) ? wp_get_attachment_image_src( $archive_options['lg-service-archive-banner'], 'full' ) : get_option( 'lg_option_blog_archive_banner_image' );

if ( ! isset( $banner ) || empty( $banner ) ) {
	$banner[0] = 'https://via.placeholder.com/1900x300/2d3258/ffffff?text=Please+Set+Default+Blog+Image+in+Site+Settings+Or+Choose+Specific+Image+On+Service+Archive+Settings+Page';
}

?>



<main>
	<div 
		class="wp-block-cover alignfull page-header" 
		style="background-image:url(<?php echo esc_attr( $banner[0] ); ?>)"
	>
		<div class="wp-block-cover__inner-container">
			<h1 class="has-dark-color has-text-color"><?php echo esc_html( $archive_options['lg-service-archive-banner-text'] ); ?></h1>
			<?php
			if ( function_exists( 'yoast_breadcrumb' ) ) {
				yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
			}
			?>
		</div>
	</div>	
	<div class="container py-5">
		<?php if ( isset( $archive_options['lg-service-archive-blurb'] ) ) : ?>
			<div class="lg-service-archive__blurb text-center pb-4">
				<?php echo esc_html( $archive_options['lg-service-archive-blurb'] ); ?> 
			</div>
		<?php endif; ?>
		<?php $service_categories = get_categories( array( 'taxonomy' => 'lg-service-category' ) ); ?>

		<?php foreach ( $service_categories as $category ) : ?> 
			<h2 class="text-center pb-2" ><?php echo esc_html( $category->name ); ?></h2>
			<?php if ( isset( $category->description ) && ! empty( $category->description ) ) : ?>
				<p class="text-center mb-3"><?php echo esc_html( $category->description ); ?></p>
			<?php endif; ?>
			<?php
			$args = array(
				'posts_per_page' => -1,
				'post_type'      => 'lg-service',
				'tax_query'      => array( //phpcs:ignore
					array(
						'taxonomy' => 'lg-service-category',
						'field'    => 'slug',
						'terms'    => $category->slug,
					),
				),
			);

			$category_posts = new WP_Query( $args );

			?>
			<div class="wp-block-lg-blocks-cards lg-cards container mb-4">
				<?php $cols = 1 === $category_posts->post_count ? 2 : ( 0 === $category_posts->post_count % 2 ? 2 : 3 ); ?>
				<div class="row row-cols-1 row-cols-md-2 row-cols-lg-<?php echo esc_attr( $cols ); ?> justify-content-center">	
					<?php if ( $category_posts->have_posts() ) : while ( $category_posts->have_posts() ) : $category_posts->the_post(); ?>
							<?php

							$excerpt = get_the_excerpt();
							$excerpt = substr( $excerpt, 0, 0 === $cols % 2 ? 200 : 125 );
							$excerpt = substr( $excerpt, 0, strrpos( $excerpt, ' ' ) );
							?>
						<div class="wp-block-lg-blocks-card col mb-3">
							<div class="card ">
								<?php echo get_the_post_thumbnail(); ?>
								<div class="card-content wp-block-lg-blocks-card col">
									<div class="card-body">
										<h3 class="has-text-align-center"><?php echo esc_html( get_the_title() ); ?></h3>

										<p class="has-text-align-center">
											<?php echo esc_html( $excerpt ); ?>...
										</p>

										<p class="has-text-align-center">
											<a href="<?php echo esc_attr( get_the_permalink() ); ?>"
												>View <?php echo esc_html( get_the_title() ); ?> </a
											>
										</p>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>

		<?php endforeach; ?>
	</main>
<?php get_footer(); ?>
